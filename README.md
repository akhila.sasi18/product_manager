

## How to clone

git clone git@gitlab.com:akhila.sasi18/product_manager.git

Change Repository

composer install

cp .env.dist .env

php artisan key:generate

php artisan migrate --seed

php artisan serve

