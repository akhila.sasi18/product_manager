<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\AdminLoginController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return new RedirectResponse('admin/login');
});
Auth::routes();

Route::prefix('admin')->group(function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
	Route::get('/login',[AdminLoginController::class,'showLoginForm'])->name('admin.login');
	Route::post('/login',[AdminLoginController::class,'login'])->name('admin.login.submit');
	Route::get('/logout',[AdminLoginController::class,'logout'])->name('admin.logout');
    Route::get('/dashboard', [AdminController::class,'index'])->name('admin.dashboard');

    // Product Routes
    Route::get('/products', [ProductController::class, 'index'])->name('products.index');
    Route::get('/products/uploadcsv', [ProductController::class, 'showUploadForm'])->name('products.showUploadForm');
    Route::post('/products/uploadcsv', [ProductController::class, 'uploadCsv'])->name('products.uploadcsv');
    Route::get('/products/import', [ProductController::class, 'import'])->name('products.improt');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
