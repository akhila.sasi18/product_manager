@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <span>{{ __('Products') }}</span>
                        <div class="d-flex">
                            <button onclick="window.location='{{ route('admin.dashboard') }}'"
                                class="btn btn-primary me-2">Dashboard</button>
                            <button onclick="window.location='{{ route('products.showUploadForm') }}'"
                                class="btn btn-primary">Upload CSV</button>
                        </div>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>SKU</th>
                                    <th>Name</th>
                                    <th>Published</th>
                                    <th>Description</th>
                                    <th>Tax Status</th>
                                    <th>Tax Class</th>
                                    <th>In Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->type }}</td>
                                        <td>{{ $product->sku }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->published ? 'Yes' : 'No' }}</td>
                                        <td>{{ $product->description }}</td>
                                        <td>{{ $product->tax_status }}</td>
                                        <td>{{ $product->tax_class }}</td>
                                        <td>{{ $product->in_stock ? 'Yes' : 'No' }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" align="center"><strong>No Product(s) to list.</strong></td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
