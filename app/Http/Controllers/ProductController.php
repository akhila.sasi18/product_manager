<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Config;

use Maatwebsite\Excel\Facades\Excel;
use App\Models\ProductImportLog;
use Illuminate\Support\Facades\Mail;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function showUploadForm(){
        return view('products.showuploadform');
    }

    public function uploadCsv(Request $request){

        $request->validate([
            'product_file' => 'required|file|mimes:csv,txt',
        ]);

        $file = $request->file('product_file');
        $fileNameToStore = $this->storeFile($file);
        return redirect()->route('admin.dashboard')->with('success', 'CSV file uploaded successfully.');
    }

    private function storeFile($file)
    {
        $destinationPath = Config::get('upload.path');
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath, 0755, true);
        }

        $fileNameToStore = time() . '_' . $file->getClientOriginalName();
        $file->move($destinationPath, $fileNameToStore);

        return $fileNameToStore;
    }


    public function import(){
        $sourceFilePath = Config::get('upload.path');
        $files = scandir($sourceFilePath);
        if (!empty($files)) {
            foreach ($files as $file) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                $filePath = $sourceFilePath . '/' . $file;

                if (is_readable($filePath)) {
                    $data = Excel::toArray([], $filePath);

                    $data = array_slice($data[0], 1);
                    $totalRows = count($data);
                    $processedRows = 0;

                    foreach ($data as $row) {
                        $type = $row[1];
                        $sku = $row[2];
                        $name = $row[3];
                        $published = $row[4];
                        $description = $row[5];
                        $tax_status = $row[6];
                        $tax_class = $row[7];
                        $in_stock = $row[8];

                        Product::create([
                            'type' => $type,
                            'sku' => $sku,
                            'name' => $name,
                            'published' => $published,
                            'description' => $description,
                            'tax_status' => $tax_status,
                            'tax_class' => $tax_class,
                            'in_stock' => $in_stock
                        ]);

                        $processedRows++;
                    }

                    $remainingRows = $totalRows - $processedRows;

                    ProductImportLog::create([
                        'file_name' => $file,
                        'error' => null,
                        'message' => 'Imported successfully',
                        'processed_rows' => $processedRows,
                        'remaining_rows' => $remainingRows
                    ]);

                    // After processing, delete the file
                    unlink($filePath);
                } else {
                    ProductImportLog::create([
                        'file_name' => $file,
                        'message' => 'Error: File is not readable'
                    ]);
                }
            }

            // Send notification only if files exist
            $adminEmail = env('ADMIN_EMAIL');
            $subject = 'Import Notification';
            $body = 'The import process has been completed successfully.';

            Mail::raw($body, function ($message) use ($adminEmail, $subject) {
                $message->to($adminEmail)
                    ->subject($subject);
            });
        }
    }
}
