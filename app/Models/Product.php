<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $primaryKey = 'product_id';

    protected $fillable = [
        'type',
        'sku',
        'name',
        'published',
        'description',
        'tax_status',
        'tax_class',
        'in_stock',
    ];
}
