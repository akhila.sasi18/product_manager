<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImportLog extends Model
{
    use HasFactory;

    protected $primaryKey ='log_id';

    protected $fillable = [
        'file_name',
        'message',
        'error',

    ];
}
